module.exports = function(app) {
  var controller = app.controllers.contato;
  var auth = app.services.auth;

  app.route('/contato')
    .get(auth.validarAutenticacao, controller.listar)
    .post(auth.validarAutenticacao, controller.salvar);

  app.route('/contato/:id')
    .get(auth.validarAutenticacao, controller.buscar)
    .delete(auth.validarAutenticacao, controller.remover);
};